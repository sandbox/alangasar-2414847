<?php

function countdown_timer_settings_form($form, &$form_state) {
  $form = array();

  $form['timer'] = array(
    '#type' => 'fieldset',
    '#title' => 'Настройки таймера',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['timer']['countdown_time'] = array(
    '#type' => 'textfield',
    '#title' => 'Часы',
    '#field_suffix' => ' час.',
    '#description' => 'Количество часов для обратного отсчета',
    '#default_value' => variable_get('countdown_time', 72),
    '#size' => 10,
    '#element_validate' => array('element_validate_number')
  );
  $today = date("Y-m-d H:i:s");
  $form['timer']['countdown_timer_start'] = array(
    '#type' => 'date_popup',
    '#title' => 'Старт',
    '#default_value' => variable_get('countdown_timer_start', $today),
    '#date_type' => DATE_DATETIME,
    '#date_timezone' => date_default_timezone(),
    '#date_format' => 'd.m.Y H:i',
    '#date_increment' => 1,
    '#date_year_range' => '-3:+3',
  );
  $form['timer']['countdown_timer_selector'] = array(
    '#type' => 'textfield',
    '#title' => 'Селектор',
    '#default_value' => variable_get('countdown_timer_selector', ''),
  );

  $format_values = array('Дни: значение - {dnn}, текст - {dl}', 'Часы: значение - {hnn}, текст - {hl}', 'Минуты: значение - {mnn}, текст - {ml}', 'Секунды: значение - {snn}, текст - {sl}');
  $form['timer']['countdown_timer_layout'] = array(
    '#type' => 'textarea',
    '#title' => 'Формат вывода',
    '#description' =>  theme('item_list', array('items' => $format_values)),
    '#default_value' => variable_get('countdown_timer_layout', '<span>{dnn}<b>{dl}</b></span><span>{hnn}<b>{hl}</b></span><span>{mnn}<b>{ml}</b></span><span>{snn}<b>{sl}</b></span>'),
  );

  $form['loading_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Загрузка таймера',
    '#weight' => 1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $match_type = array(
    0 => t('All pages except those listed'),
    1 => t('Only the listed pages'),
  );

  $form['loading_settings']['countdown_timer_pages_match'] = array(
    '#type' => 'radios',
    '#title' => t('Attach Countdown timer on specific pages'),
    '#default_value' => variable_get('countdown_timer_pages_match', 0),
    '#options' => $match_type,
  );
  $form['loading_settings']['countdown_timer_pages_path'] = array(
    '#type' => 'textarea',
    '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
    '#default_value' => variable_get('countdown_timer_pages_path'),
  );


  return system_settings_form($form);
}