jQuery(document).ready(function($){

    var start = new Date();
    start = new Date(Drupal.settings.countdown_timer.start);
    $(Drupal.settings.countdown_timer.selector).countdown({until: start, compact: false, layout: Drupal.settings.countdown_timer.layout, description: '', format: 'HMS'});
});

